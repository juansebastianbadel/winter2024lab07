public class Card {
	private String suit;
	private String value;

	public Card(String suit, String value) {
		// for the future, validate values passed
		this.suit = suit;
		this.value = value;
	}
	public String getSuit() {
		return this.suit;
	}

	public String getValue() {
		return this.value;
	}

	public String toString() {
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){
		//initializing a double for the score
		double rankScore = 0.0;
		String[] values = {"ACE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "JACK", "QUEEN", "KING"};
	
		//if the value of the card is in the array, the score will be that index plus one, since arrays start at 0
		for(int i = 0; i < values.length; i++){
			if(value.equals(values[i])){
				rankScore += i + 1.0;
			}
		}
		//adding corresponding point for each suit value
		if(suit.equals("HEARTS")){
			rankScore += 0.4;
		}
		else if(suit.equals("SPADES")){
			rankScore += 0.3;
		}
		else if(suit.equals("DIAMONDS")){
			rankScore += 0.2;
		}
		else{
			rankScore += 0.1;
		}
		
		return rankScore;
	}
}
