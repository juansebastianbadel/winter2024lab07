import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;

	public Deck() { // constructor
		this.rng = new Random();
		//System.out.println(this.rng.nextInt());
		
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards];

		String[] values = {"ACE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "JACK", "QUEEN", "KING"};
		String[] suits = {"HEARTS", "SPADES", "DIAMONDS", "CLUBS"};
		String currentSuit = "";
		
		int index = 0;
		for (int j = 0; j < suits.length; j++) {
			for (int i = 0; i < values.length; i++) {
				cards[index] = new Card(suits[j], values[i]);
				index++;
			}
		}
	}

	public int length() {
		return this.numberOfCards;
	}

	public Card drawTopCard() {
		this.numberOfCards--;
		return cards[numberOfCards];
	}

	public String toString() {
		String all = "Remaining cards: " + "\n";
		for (int i = 0; i < numberOfCards; i++) {
			all += cards[i] + "\n";
		}
		return all;
	}
	
	public void shuffle() {
		for(int i = 0; i < numberOfCards; i++){
			int randNumber = i + this.rng.nextInt(numberOfCards-i);

			Card first = cards[i];
			cards[i] = cards[randNumber];
			cards[randNumber] = first;
		}
	}
}
