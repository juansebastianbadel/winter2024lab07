public class SimpleWar{
	
	public static final String CYAN = "\u001B[36m";
	public static final String WHITE = "\u001B[37m";
	public static final String RED = "\u001B[31m";
	
	public static void main(String[] args) {
		
		//making a deck object and calling the shuffle method on it
		Deck deck = new Deck();
		deck.shuffle();
		
		int player1 = 0;
		int player2 = 0;
		
		
		//while loop that runs until the deck's length is 0
		while(deck.length() > 0){
			
			//drawing the top card and putting it in card variables
			Card card1 = deck.drawTopCard();
			Card card2= deck.drawTopCard();
			
			System.out.println("P1: " + card1 + ", " + card1.calculateScore());
			System.out.println("P2: " + card2 + ", " + card2.calculateScore());
			
			System.out.println("player 1 points: " + CYAN + player1 + WHITE);
			System.out.println("player 2 points: " + RED + player2 + WHITE);
			
			//whichever score is bigger is the winner
			if(card1.calculateScore() > card2.calculateScore()){
				System.out.println("PLAYER 1 WINS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				player1++;	
			}else{
				System.out.println("PLAYER 2 WINS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				player2++;
			}
			System.out.println("player 1 points: " + CYAN + player1 + WHITE);
			System.out.println("player 2 points: " + RED + player2 + WHITE + "\n");
		}
		
		//if the point variables have the same value, it will print draw, otherwise whichever was the highest gets printed congrats
		if(player1 > player2){
			System.out.println("Congrats to player 1!");
		}else if (player2 > player1){
			System.out.println("Congrats to player 2!");
		}else{
			System.out.println("Draw...");
		}
	}
}